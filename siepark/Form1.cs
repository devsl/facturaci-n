﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace siepark
{
    public partial class Form1 : Form
    {
        public static string sqlConnString = @"server=delllatitude\sqlexpress;database=mulprk;Trusted_Connection=True";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void exportToCSVfile(string fileOut, string query)
        {
            // Connects to the database, and makes the select command.
            SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
            //string sqlQuery = "select * from VIEW_LESHALLES_FRANCHISS";
            string sqlQuery = query;
            SqlCommand command = new SqlCommand(sqlQuery, conn);
            conn.Open();

            // Creates a SqlDataReader instance to read data from the table.
            SqlDataReader dr = command.ExecuteReader();

            // Retrieves the schema of the table.
            DataTable dtSchema = dr.GetSchemaTable();

            //string fo= fileOut.Replace("\\", @"\") ;
            var dir = new DirectoryInfo(@ConfigurationManager.AppSettings["Folder"].ToString());

            foreach (var file in dir.EnumerateFiles(fileOut + "*"))
            {
                file.Delete();
            }
            //File.Delete(fileOut);

            string strRow; // represents a full row

            // Writes the column headers if the user previously asked that.
            //if (this.chkFirstRowColumnNames.Checked)
            //{
            //    sw.WriteLine(columnNames(dtSchema, this.separator));
            //}

            // Reads the rows one by one from the SqlDataReader
            // transfers them to a string with the given separator character and
            // writes it to the file.
            while (dr.Read())
            {
                // Creates the CSV file as a stream, using the given encoding.
                StreamWriter sw = new StreamWriter(@ConfigurationManager.AppSettings["Folder"].ToString() + fileOut + dr.GetValue(1).ToString() + dr.GetValue(2).ToString(), true, Encoding.ASCII);

                strRow = "";
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    //strRow += dr.GetString(i);
                    strRow += dr.GetValue(i).ToString().TrimEnd().Replace(',','.');
                    if (i < dr.FieldCount - 1)
                    {
                        strRow += ";";
                    }
                }
                sw.WriteLine(strRow);
                sw.Close();
            }

            // Closes the text stream and the database connection.
            
            conn.Close();

            // Notifies the user.
            MessageBox.Show("Hecho!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string date = dateTimePicker2.Value.ToString("ddMMyyyy");
            string date2 = dateTimePicker2.Value.ToString("yyyyMMdd");
            string query1 = "SELECT '" + date + "' " +
                              ", RIGHT(REPLICATE('0', 4) + CAST([NoSite] AS VARCHAR(7)), 4) " +
                              ",RIGHT(REPLICATE('0', 4) + CAST([NoParc] AS VARCHAR(7)), 4)," +
                              "'1' as TipCliente" +
                              ",'' as nif" +
                              ",cast(round(sum([Montant])/100/1.21,2,0) as decimal(10,2)) as total" +
                              ",CASE" +
                              "      WHEN TypePaiment = 'Espèces' THEN 'C'" +
                              "      WHEN TypePaiment = 'CB' THEN 'T'" +
                              "END as TypeP," +
                              " ''," +
                              "''" +
                          "FROM[mulprk].[dbo].[VIEW_LESHALLES_PAIEMENT]" +
                          "WHERE Famille = 'A' and TypePaiment <> '-'" +
                          "and Date = '" + date2 + "'" +
                          "group by TypePaiment, [NoSite],[NoParc],[Famille],[TypePaiment]";
            exportToCSVfile("HPARSAPFAC" +date + "R", query1);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DateTime date = dateTimePicker1.Value;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            string primerdia = firstDayOfMonth.ToString("yyyyMMdd");
            string ultimdia = lastDayOfMonth.ToString("yyyyMMdd");
            string ultimdia2 = lastDayOfMonth.ToString("ddMMyyyy");

            string query1 = "  SELECT '" + ultimdia2 + "'" +
                  ", RIGHT(REPLICATE('0', 4) + CAST([NoSite] AS VARCHAR(7)), 4)" +
	              ",RIGHT(REPLICATE('0', 4) + CAST([NoParc] AS VARCHAR(7)), 4)," +
	              "'2' as TipCliente" +
	              ",fax_cor as nif" +
                  ",cast(round(sum([Montant])/100/1.21,2,0) as decimal(10,2)) as total" +
                  ",CASE" +
                  "      WHEN TypePaiment = 'Espèces' THEN 'C'" +
                  "       WHEN TypePaiment = 'CB' THEN 'T'" +
                  "END as TypeP" +
                  ",'\"' + rtrim(NOM_CAT) + '\"' " +
              "FROM [mulprk].[dbo].[VIEW_LESHALLES_PAIEMENT] " +
              "JOIN mulprk.dbo.CAR on NoCarte = n_car " +
              "JOIN mulprk.dbo.cat on mulprk.dbo.car.n_cat = mulprk.dbo.cat.N_CAT " +
              "JOIN mulprk.dbo.cnt on mulprk.dbo.car.n_cnt = mulprk.dbo.cnt.n_cnt " +
              "JOIN mulprk.dbo.corr on mulprk.dbo.cnt.n_sou = mulprk.dbo.corr.n_sou " +
              "WHERE Famille <> 'A' and TypePaiment <> '-' " +
              "and Date between '" + primerdia + "' and '" + ultimdia + "' " +
              "group by TypePaiment, [NoSite],[NoParc],NoCarte" +
                  ",[Famille]" +
                 ",[TypePaiment],NOM_CAT, fax_cor";
            exportToCSVfile("HPARSAPFAC" + ultimdia2 + "A", query1);

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
